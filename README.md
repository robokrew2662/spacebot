# Robokrew Code 2019
Robokrew code repo for FRC game Deep Space 2019
The robot is able to suck up balls(cargo) using a rollers, use an elevator to move up and down and finally spit out the cargo into the goal. Also a pneumatic is used to grab the hatch panels and score them using similar

## How to get Code onto the Robot from Gitlab Repo
1. Confirm your code is committed in the gitlab repo
2. Open git-bash tool found on the C drive of the team laptop
3. Change directory to the code directory on the team laptop
	1. cd  /C/RoboKew/2019Code/2019Code2
4. fetch latest meta data about repo (new branches for example)
	  1. git fetch
5. checkout to the branch you would like to deploy
	  1. git checkout \<branch-Name>
6. pull down the latest code
	  1. git pull
7. Open up VS code on team laptop
8. Connect Laptop to robot(ethernet,wirelessly,etc)
9. Run the WPI deploy
10. Drink Dew and pray it's gonna work

## How to get Code From team laptop to GitLab
1. Open git-bash tool found on the C drive of the team laptop
2. Change directory to the code directory on the team laptop
	1. cd  /C/RoboKew/2019Code/2019Code2
3. Confirm you on the right branch (see "check out steps" above)
4. Staged the Changed files
	  1. git add *
	  2. git commit *
7. push the code to gitlab
	  1. git push