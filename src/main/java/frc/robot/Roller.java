/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.GenericHID;

public class Roller {
    private Spark rollerMotor;//Motor for rollerMotor
    private static final double FORWARDOUTTAKE = .5;//supposed to be faster because its shooting
    private static final double FORWARDINTTAKE = .5;//supposed to be slower because its picking up balls
    private static final double BACKWARDSOUTTAKE = -0.5;
    private static final boolean WEHASBALL = true;
    private DigitalInput ballIn = new DigitalInput(7); //ball sensor
    private int sensorCounter = 0;
    private static int LENGTHOFDELAY = 0; //how many loops we wait to stop the motor
    private static boolean alreadyPressed = false;//tells you if the button has already been pressed to prevent unwanted function if the button is held down
    private boolean startMotor = false;

    public Roller(Spark sMotor)
    {
        //System.out.println("Inside Roller");
        rollerMotor = sMotor;
    }

    public void controlRoller (XboxController rollerController) 
    {
        //System.out.println("Roller Channel:"+rollerMotor.getChannel());
        //System.out.println("Counter:"+sensorCounter+",LENGTH:"+LENGTHOFDELAY);
        //System.out.println("XButton:"+rollerController.getXButtonPressed());
        //basically we want the roller to intake while holding down x button until the limit is pressed and we want it to shoot out if the trigger is pressed 
        if(rollerController.getXButtonPressed() && (alreadyPressed == false))
        {
            System.out.println("X Button Pressed"); 
            startMotor = !startMotor;
            alreadyPressed = true;
        }else if (rollerController.getXButtonPressed() == false){
            alreadyPressed = false;
        }

        //System.out.println("ballSensor:"+ballIn.get()+ "StartMotor:"+startMotor);
        if (ballIn.get() != WEHASBALL && startMotor == true ){
            System.out.println("INTAKE");
            rollerMotor.set(FORWARDINTTAKE);
        }else if(rollerController.getTriggerAxis(GenericHID.Hand.kRight) > 0){
            System.out.println("SHOOT DA BALL");
            rollerMotor.set(FORWARDOUTTAKE);
            sensorCounter = 0;
            startMotor=true;
        }else if(rollerController.getTriggerAxis(GenericHID.Hand.kLeft) > 0){
            System.out.println("SHOOT BACKWARDS");
            rollerMotor.set(BACKWARDSOUTTAKE);
            sensorCounter = 0;
            startMotor = true;
        }else{ // sensor is tripped and no button pressed
          
            // if (sensorCounter == LENGTHOFDELAY )
            //{
                rollerMotor.set(0);
                startMotor=false;
                
                System.out.println("STOP INTAKE SENSOR");
            //}
            //else 
            //{
            //    sensorCounter++;
            //}
            //rollerMotor.set(rollerController.getRawAxis(1));
        }
        
    }
}
