/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.cameraserver.*;
import frc.robot.Elevator;
import frc.robot.Pincher;


public class Robot extends TimedRobot {
  private final SpeedControllerGroup leftMotors = new SpeedControllerGroup(new Spark(0),new Spark(2));
  private final SpeedControllerGroup rightMotors = new SpeedControllerGroup(new Spark(1), new Spark(3));
  private final DifferentialDrive robotDrive= new DifferentialDrive(leftMotors, rightMotors);
  private final XboxController driveController = new XboxController(0);
  private final XboxController controllerOfThe2ndDegree = new XboxController(1);
  private final Timer m_timer = new Timer();
  private final Elevator  elevator =  new Elevator(new Spark(4));
  private final Roller roller = new Roller(new Spark(6));
  private final Solenoid theeLever = new Solenoid(1);
  private static boolean hasButtonBeenPressed = false;
  private static boolean RETRACT = false;
  private static boolean EXTEND = true;
  private static Pincher pincher = new Pincher(new Solenoid(0));
  private double curJoyValue;
  private double speedChange;
  

  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
       CameraServer.getInstance().startAutomaticCapture();
  }

  /**
   * This function is run once each time the robot enters autonomous mode.
   */
  @Override
  public void autonomousInit() {
     theeLever.set(EXTEND);
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    robotDrive.arcadeDrive(driveController.getRawAxis(1) * .666, -driveController.getRawAxis(4)*.666, driveController.getYButton());
    elevator.controlElevator(controllerOfThe2ndDegree);
    roller.controlRoller(controllerOfThe2ndDegree);
    pincher.controlPincher(controllerOfThe2ndDegree);
  }

  /**
   * This function is called once each time the robot enters teleoperated mode.
   */
  @Override
  public void teleopInit() {
    theeLever.set(false);
     curJoyValue = 0;//current joystick value
     speedChange = 0; // what we are changing the motor by
  }

  /**
   * This function is called periodically during teleoperated mode.
   */
  @Override
  public void teleopPeriodic() {
 
 
  double newDrivePos = driveController.getRawAxis(1); //desired
  double MAXCHANGE = .03; //max change value
  double speedChange = newDrivePos - curJoyValue;
  if(newDrivePos != curJoyValue) {
    if(speedChange >= MAXCHANGE){
      speedChange = MAXCHANGE;
    } else if (speedChange <= -MAXCHANGE){
      speedChange = -MAXCHANGE;
    }
  }

  curJoyValue = curJoyValue + speedChange;
  //curJoyValue=newDrivePos;

  System.out.println("CUR JOYVALUE:"+curJoyValue);

    robotDrive.arcadeDrive(curJoyValue, -driveController.getRawAxis(4)*.666, driveController.getYButton());
    elevator.controlElevator(controllerOfThe2ndDegree);
    roller.controlRoller(controllerOfThe2ndDegree);
    pincher.controlPincher(controllerOfThe2ndDegree);

    theeLever.set(false);
    
    if(controllerOfThe2ndDegree.getRawButton(5) && theeLever.get() == EXTEND && (hasButtonBeenPressed == false)){
      System.out.println("RETRACT Lever"); 
      hasButtonBeenPressed = true;
      theeLever.set(RETRACT);
    }else if(controllerOfThe2ndDegree.getRawButton(5) && (theeLever.get() == RETRACT) && (hasButtonBeenPressed == false)){
      System.out.println("EXTEND Lever"); 
      theeLever.set(EXTEND);
      hasButtonBeenPressed = true;
    }else if(controllerOfThe2ndDegree.getRawButton(5) == false){
      hasButtonBeenPressed = false;
    }
  }

  /**            
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}