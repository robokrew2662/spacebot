/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.Solenoid;

public class Pincher  {

    private final Solenoid pincherSolenoid;//The new solunoid
    private static boolean alreadyPressed = false;//tells you if the button has already been pressed to prevent unwanted function if the button is held down

    private long timeAtFirstPress;
    private static final long buttonDelayInMs = 300;

    public Pincher(Solenoid sSolunoid){
        pincherSolenoid = sSolunoid;
    }

    public void controlPincher (XboxController pincherController)
    {
        //System.out.println("Solenoid Pincher:"+pincherSolenoid.get()+"; Pincher Button:"+ pincherController.getBButton() + "alreadyPressed:" + alreadyPressed  );
        if (pincherController.getBButton() && pincherSolenoid.get() && (alreadyPressed == false)){
            System.out.println("OPEN Pincher"); 
            alreadyPressed = true;
            pincherSolenoid.set(false);
        }else if (pincherController.getBButton() && (pincherSolenoid.get() == false) && (alreadyPressed == false)){
            System.out.println("CLOSE Pincher"); 
            pincherSolenoid.set(true);
            alreadyPressed = true;
        }else if (pincherController.getBButton() == false){
            alreadyPressed = false;
        }

    }
}
