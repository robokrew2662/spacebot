package frc.robot;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.XboxController;

public class Elevator{

     // Our controller
    int currentHeight; // PLACEHOLDER (we don't know what to use for this) 
    private static final double UP = .5;
    private static final double DOWN = -.5;
    private static final boolean LIMITPRESSED = true;
    private static final boolean LIMITUNPRESSED = false;

    //Encoder elevatorEncoder = new Encoder(0,1);//Encoder
    
    private DigitalInput highLimit = new DigitalInput(1); //High Switch 
    private DigitalInput lowLimit = new DigitalInput(2); //lowLimit
    private Spark elevatorMotor; 

    public Elevator(Spark sparkMotor)
    {
        //System.out.println("Inside Elevator");
        elevatorMotor = sparkMotor; //The motor for elevator
    }

    public void controlElevator(XboxController elevatorController)
    {
        //System.out.println("YButton:"+elevatorController.getYButton()+",highLimit:"+highLimit.get());
    if ((elevatorController.getYButton() == true) && (highLimit.get() == LIMITUNPRESSED)){
        System.out.println("Moving UP"); 
        elevatorMotor.set(UP);
    }
    else if((elevatorController.getAButton() == true) && (lowLimit.get() == LIMITUNPRESSED)){
       System.out.println("Moving DOWN"); 
        elevatorMotor.set(DOWN);
    }
    else{
        //System.out.println("STOP");
        elevatorMotor.set(0);
    }
}
    
}